# DSpace minimal Front



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:


```
cd existing_repo
git remote add origin https://gitlab.com/ricardoortiz.salud/dspace-minimal-front.git
git branch -M main
git push -uf origin main
```

***
# Quitar Collections y botón de vusualización de propiedades del item

Quitar las siguientes líneas en:

./src/app/item-page/simple/item-types/untyped-item/untyped-item.component.html


```
    <ds-item-page-collections [item]="object"></ds-item-page-collections>
    <div>
      <a class="btn btn-outline-primary" [routerLink]="[itemPageRoute + '/full']" role="button">
        <i class="fas fa-info-circle"></i> {{"item.page.link.full" | translate}}
      </a>
    </div>
```
# Agregar campo Source en página del item

Agregar en:

./src/app/item-page/simple/item-types/untyped-item/untyped-item.component.html

Las siguientes líneas:

```
<ds-item-page-uri-field [item]="object"
    [fields]="['dc.source']"
    [label]="'item.page.source'">
```

Agregar en:

./src/assets/i18n/es.json5 

La siguiente línea:

```
"item.page.source": "Enlace de descarga",
```

# Agregar campo URI a la previsualización del item en Mi DSpace

Agregar en:

./src/app/shared/object-list/my-dspace-result-list-element/item-list-preview/item-list-preview.component.html

Después de la línea:

```
  <h3 [innerHTML]="dsoTitle" [ngClass]="{'lead': true,'text-muted': !item.firstMetadataValue('dc.title')}"></h3>
```

Las siguientes líneas:

```
    <a [routerLink]="['/handle/'] + item.handle">  
      <span *ngIf="item.hasMetadata('dc.identifier.uri')" class="item-page-uri-field" 
      [innerHTML]="item.firstMetadataValue('dc.identifier.uri')">
      
      </span>
    </a>

```
Modificar las siguientes líneas en:

./src/assets/i18n/es.json5 


```

# Traducción de estado del item en Mi DSpace

  // "mydspace.show.supervisedWorkspace": "Supervised items",
  "mydspace.show.supervisedWorkspace": "Ítems supervisados",

  // "mydspace.status.mydspaceArchived": "Archived",
  "mydspace.status.mydspaceArchived": "Archivado",

  // "mydspace.status.mydspaceValidation": "Validation",
  "mydspace.status.mydspaceValidation": "Validación",

  // "mydspace.status.mydspaceWaitingController": "Waiting for controller",
  "mydspace.status.mydspaceWaitingController": "Esperando al controlador",

  // "mydspace.status.mydspaceWorkflow": "Workflow",
  "mydspace.status.mydspaceWorkflow": "Flujo de trabajo",

  // "mydspace.status.mydspaceWorkspace": "Workspace",
  "mydspace.status.mydspaceWorkspace": "Espacio de trabajo",

```
